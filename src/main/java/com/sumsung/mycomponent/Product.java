/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sumsung.mycomponent;

import java.util.ArrayList;

/**
 *
 * @author sumsung
 */
public class Product {
    private int id;
    private String name;
    private double price;
    private String image;

    @Override
    public String toString() {
        return "Product{" + "id=" + id + ", name=" + name + ", price=" + price + ", image=" + image + '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Product(int id, String name, double price, String image) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.image = image;
    }
    
    public static ArrayList<Product> genProductList(){
        ArrayList<Product> list = new ArrayList<>();
        list.add(new Product(1,"เอสเพรสโซ่1",40,"1.jpeg"));
        list.add(new Product(2,"เอสเพรสโซ่2",50,"1.jpeg"));
        list.add(new Product(3,"เอสเพรสโซ่3",40,"1.jpeg"));
        list.add(new Product(4,"ชาไทย1",60,"3.jpeg"));
        list.add(new Product(5,"ชาไทย2",40,"3.jpeg"));
        list.add(new Product(6,"ชาไทย3",30,"3.jpeg"));
        list.add(new Product(7,"ลาเต้1",50,"1.jpeg"));
        list.add(new Product(8,"ชาเขียว1",60,"4.jpeg"));
        list.add(new Product(9,"ชาเขียว2",50,"4.jpeg"));
        return list;
    }
}
